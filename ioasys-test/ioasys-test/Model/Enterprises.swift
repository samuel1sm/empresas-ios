//
//  Enterprises.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 22/04/22.
//

import Foundation

struct Enterprises: Codable {
    let enterprises: [Enterprise]

    enum CodingKeys: String, CodingKey {
        case enterprises
    }
}

struct Enterprise: Codable {
    let id: Int
    let emailEnterprise: String?
    let facebook: String?
    let twitter: String?
    let linkedin: String?
    let phone: String?
    let ownEnterprise: Bool
    let enterpriseName: String
    let photo: String
    let description: String
    let city: String
    let country: String
    let value: Int
    let sharePrice: Float
    let enterpriseType: EnterpriseType

    enum CodingKeys: String, CodingKey {
        case id
        case emailEnterprise = "email_enterprise"
        case enterpriseName = "enterprise_name"
        case photo
        case description
        case city
        case country
        case value
        case enterpriseType = "enterprise_type"
        case sharePrice = "share_price"
        case facebook
        case twitter
        case linkedin
        case phone
        case ownEnterprise = "own_enterprise"

    }
}

struct EnterpriseType: Codable {
    let id: Int
    let enterpriseTypeName: String

    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseTypeName = "enterprise_type_name"
    }
}
