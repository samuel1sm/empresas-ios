//
//  RequestHeaders.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 21/04/22.
//

import Foundation

struct RequestHeaders: Codable {
    let uid: String
    let client: String
    let accessToken: String

    func getHeaders() -> [String: String] {
        return ["uid": uid, "client": client, "access-token": accessToken, "Content-Type": "application/json"]
    }
}
