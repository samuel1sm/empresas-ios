//
//  Login.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 20/04/22.
//

import Foundation

struct Account: Codable {
    let email: String
    let password: String
}
