//
//  MainCoordinator.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 18/04/22.
//

import UIKit

class MainCoordinator: Coordinator {
    var navigationController: UINavigationController?

    func start() {
        let viewController = LoginViewController()
        let loginService: LoginRequestProtocol = LoginService()
        viewController.viewModel = LoginViewModel(loginService: loginService)
        viewController.coordinator = self

        navigationController?.setViewControllers([viewController], animated: false)
    }

    func next(screen: Screens) {
        switch screen {
        case .enterprises(let headers):
            openEnterprises(headers: headers)
        case .details(let enterprise):
            openDetail(detail: enterprise)
        }
    }

    private func openEnterprises(headers: RequestHeaders ) {
        let viewController = EnterprisesViewController()
        let enterpriseService: EnterprisesService = EnterprisesService()

        viewController.viewModel = EnterprisesViewModel(service: enterpriseService, headers: headers)
        viewController.coordinator = self
        guard let loginViewController = navigationController?.topViewController else {return}
        navigationController?.setViewControllers([viewController], animated: true)
        loginViewController.removeFromParent()
    }

    private func openDetail(detail: Enterprise ) {
        let viewController = DetailsViewController()
        viewController.enterprise = detail

        navigationController?.pushViewController(viewController, animated: true)
    }

}
