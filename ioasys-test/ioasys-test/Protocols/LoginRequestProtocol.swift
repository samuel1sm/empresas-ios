//
//  LoginProtocol.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 20/04/22.
//

import Foundation
import Alamofire

protocol LoginRequestProtocol {
    func makeLogin(account: Account, completion: @escaping (AFDataResponse<Data?>) -> Void)
}
