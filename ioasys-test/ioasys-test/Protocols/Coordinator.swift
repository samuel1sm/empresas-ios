//
//  Coordinator.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 18/04/22.
//

import UIKit

enum Screens {
    case enterprises(headers: RequestHeaders)
    case details(enterprise: Enterprise)
}

protocol Coordinator {
    var navigationController: UINavigationController? {get set}

    func start()
    func next(screen: Screens)
}
