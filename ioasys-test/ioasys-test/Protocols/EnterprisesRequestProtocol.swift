//
//  EnterprisesRequestProtocol.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 23/04/22.
//

import Foundation
import Alamofire

protocol EnterprisesRequestProtocol {
    func getEnterprises(headers: RequestHeaders, completion: @escaping (DataResponse<Enterprises, AFError>) -> Void)
}
