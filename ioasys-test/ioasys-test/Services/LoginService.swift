//
//  LoginService.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 20/04/22.
//

import Foundation
import Alamofire

class LoginService: LoginRequestProtocol {
    func makeLogin(account: Account, completion: @escaping (AFDataResponse<Data?>) -> Void) {
        AF.request("\(Constants.baseUrl)/api/v1/users/auth/sign_in", method: .post,
                   parameters: account, encoder: JSONParameterEncoder.default) { _ in
        }.response(completionHandler: completion)
    }

}
