//
//  EnterprisesService.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 23/04/22.
//

import Foundation
import Alamofire

class EnterprisesService: EnterprisesRequestProtocol {
    func getEnterprises(headers: RequestHeaders, completion: @escaping (DataResponse<Enterprises, AFError>) -> Void) {
        AF.request("\(Constants.baseUrl)/api/v1/enterprises", method: .get) { request in
            request.headers = HTTPHeaders(headers.getHeaders())
            print(request.headers)
        }.responseDecodable(of: Enterprises.self, completionHandler: completion)
    }

}
