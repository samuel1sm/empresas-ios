//
//  EnterprisesViewModel.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 22/04/22.
//

import Foundation
import RxSwift
import RxRelay

class EnterprisesViewModel {
    private let service: EnterprisesService
    private let headers: RequestHeaders
    private(set) var selectedEnterprises: PublishRelay<[Enterprise]> = PublishRelay<[Enterprise]>()
    private var enterprises: [Enterprise]?
    private var filteredEnterprises: [Enterprise] = []

    init(service: EnterprisesService, headers: RequestHeaders) {
        self.service = service
        self.headers = headers
    }

    func findByPosition(position: Int) -> Enterprise {
        return filteredEnterprises[position]
    }

    func filter(by query: String) {
        guard let enterprises = enterprises else {
            return
        }

        if query.isEmpty {
            filteredEnterprises = enterprises
            selectedEnterprises.accept(enterprises)
            return
        }

        filteredEnterprises = enterprises.filter({ enterprise in
            return enterprise.enterpriseName.lowercased().contains(query.lowercased())
        })

        selectedEnterprises.accept(filteredEnterprises)
    }

    func fetchEnterprises() {
        service.getEnterprises(headers: headers) { [weak self] response in
            guard let enterprises = response.value else { return }
            self?.filteredEnterprises = enterprises.enterprises
            self?.enterprises = enterprises.enterprises
            self?.selectedEnterprises.accept(enterprises.enterprises)
        }

    }

}
