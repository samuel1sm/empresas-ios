//
//  File.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 20/04/22.
//

import Foundation
import RxRelay

class LoginViewModel {
    private let loginService: LoginRequestProtocol
    private(set) var loggedIn = PublishRelay<Bool>()
    var headers: RequestHeaders?

    init(loginService: LoginRequestProtocol) {
        self.loginService = loginService
    }

    func login(email: String, password: String) {
        let account = Account(email: email, password: password)
        loginService.makeLogin(account: account) { [weak self] result in

            guard let headers = result.response?.headers else { return }

            guard let uid = headers.value(for: "uid"),
                  let client = headers.value(for: "client"),
                  let token = headers.value(for: "access-token")
            else {
                self?.loggedIn.accept(false)
                return
            }

            self?.headers = RequestHeaders(uid: uid,
                                           client: client,
                                           accessToken: token)

            self?.loggedIn.accept(true)
        }
    }
}
