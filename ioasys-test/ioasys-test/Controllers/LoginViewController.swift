//
//  ViewController.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 14/04/22.
//

import UIKit
import RxCocoa
import RxSwift
import RxRelay
import Kingfisher
class LoginViewController: UIViewController {
    var coordinator: MainCoordinator?
    var viewModel: LoginViewModel?
    let disposebag = DisposeBag()

    let bottomView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()

    let confirmButtom: UIButton = {
        let view = UIButton()
        view.backgroundColor = .gray
        view.setTitle("ENTRAR", for: .normal)
        view.layer.cornerRadius = 20
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isEnabled = false
        return view
    }()

    let emailTextField: LoginUITextField = {
        let view = LoginUITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textfieldType = .emailAddress
        view.placeholderText = "Email"
        view.bottonText = "Endereço de email inválido"
        return view
    }()

    let passwordTextField: LoginUITextField = {
        let view = LoginUITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textfieldType = .password
        view.placeholderText = "Senha"
        return view
    }()

    let loadingView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImageView()

        let path = Bundle.main.url(forResource: Constants.Images.loading, withExtension: "gif")!
        let resource = LocalFileImageDataProvider(fileURL: path)
        image.kf.setImage(with: resource)

        image.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(image)

        NSLayoutConstraint.activate([
            image.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            image.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            image.heightAnchor.constraint(equalTo: image.widthAnchor),
            image.widthAnchor.constraint(equalToConstant: 50)
        ])

        view.isHidden = true
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        configureBackground()
        configureStaticComponents()
        configureBottomArea()
        configureLoadingView()
        setupBinds()

    }

    private func setupBinds() {
        bindKeyboardEnterButton()
        bindTextboxTopTextStatus()
        binTextboxSelectedColor()
        bindConfirmButtom()
        bindViewModel()
    }

    private func bindKeyboardEnterButton() {
        emailTextField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe { [weak self] _ in
                if let isComplete = self?.isTextCompleted(text: self?.passwordTextField.text), !isComplete {
                    self?.passwordTextField.textField.becomeFirstResponder()
                }
            }.disposed(by: disposebag)

        passwordTextField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe { [weak self] _ in
                if let isComplete = self?.isTextCompleted(text: self?.emailTextField.text), !isComplete {
                    self?.emailTextField.textField.becomeFirstResponder()
                }
            }.disposed(by: disposebag)
    }

    private func binTextboxSelectedColor() {
        emailTextField.textField.rx.controlEvent([.editingDidBegin])
            .subscribe { [weak self] _ in
                self?.emailTextField.isSelected(true)
            }.disposed(by: disposebag)

        passwordTextField.textField.rx.controlEvent([.editingDidBegin])
            .subscribe { [weak self] _ in
                self?.passwordTextField.isSelected(true)
            }.disposed(by: disposebag)

        emailTextField.textField.rx.controlEvent([.editingDidEnd])
            .subscribe { [weak self] _ in
                self?.emailTextField.isSelected(false)
                self?.activateConfirmButton()
            }.disposed(by: disposebag)

        passwordTextField.textField.rx.controlEvent([.editingDidEnd])
            .subscribe { [weak self] _ in
                self?.passwordTextField.isSelected(false)
                self?.activateConfirmButton()
            }.disposed(by: disposebag)
    }

    private func bindTextboxTopTextStatus() {
        emailTextField.textField.rx.controlEvent([.editingChanged])
            .asObservable().subscribe({ [weak self] _ in
                guard let text = self?.emailTextField.text else {
                    self?.emailTextField.activateTopText(false)
                    return
                }

                self?.emailTextField.activateTopText(!text.isEmpty)
            }).disposed(by: disposebag)

        passwordTextField.textField.rx.controlEvent([.editingChanged])
            .asObservable().subscribe({ [weak self] _ in
                guard let text = self?.passwordTextField.text else {
                    self?.passwordTextField.activateTopText(false)
                    return
                }

                self?.passwordTextField.activateTopText(!text.isEmpty)
            }).disposed(by: disposebag)
    }

    private func bindConfirmButtom() {
        confirmButtom.rx.tap.asObservable().subscribe({ [weak self] _ in
            guard let emailText = self?.emailTextField.text else { return }
            guard let passwordText = self?.passwordTextField.text else { return }

            self?.loadingView.isHidden = false
            self?.viewModel?.login(email: emailText, password: passwordText)

        }).disposed(by: disposebag)
    }

    private func activateConfirmButton() {
        guard let emailText = emailTextField.text else {return}
        guard let passwordText = passwordTextField.text else {return}
        if emailText.isEmpty || passwordText.isEmpty { return }

        self.confirmButtom.isEnabled = true
        self.confirmButtom.backgroundColor = .black
    }

    private func isTextCompleted(text: String?) -> Bool {
        guard let text = text else {return false}
        return !text.isEmpty
    }

    private func bindViewModel() {
        self.viewModel?.loggedIn.asDriver(onErrorDriveWith: Driver.empty()).drive(onNext: { [weak self] wasLogged in
            if wasLogged {
                guard let headers = self?.viewModel?.headers else {return}

                self?.coordinator?.next(screen: .enterprises(headers: headers))
                return
            }
            self?.loadingView.isHidden = true

            self?.emailTextField.isTextWrong(true)
        }).disposed(by: disposebag)
    }

    private func configureBottomArea() {
        let infoText = UILabel.gilroy(type: .medium, text: "Digite seus dados para continuar.",
                                      fontSize: 18, color: .black)

        infoText.translatesAutoresizingMaskIntoConstraints = false

        self.bottomView.addSubview(infoText)
        self.bottomView.addSubview(emailTextField)
        self.bottomView.addSubview(passwordTextField)
        self.bottomView.addSubview(confirmButtom)

        NSLayoutConstraint.activate([
            infoText.leadingAnchor.constraint(equalTo: bottomView.safeAreaLayoutGuide.leadingAnchor, constant: 24),
            infoText.trailingAnchor.constraint(equalTo: bottomView.safeAreaLayoutGuide.trailingAnchor, constant: -24),
            infoText.topAnchor.constraint(equalTo: bottomView.safeAreaLayoutGuide.topAnchor, constant: 24)
        ])

        NSLayoutConstraint.activate([
            emailTextField.leadingAnchor.constraint(equalTo:
                                                        bottomView.safeAreaLayoutGuide.leadingAnchor, constant: 24),
            emailTextField.trailingAnchor.constraint(equalTo:
                                                        bottomView.safeAreaLayoutGuide.trailingAnchor, constant: -24),
            emailTextField.topAnchor.constraint(equalTo: infoText.safeAreaLayoutGuide.bottomAnchor, constant: 32),
            emailTextField.heightAnchor.constraint(equalToConstant: 110)
        ])

        NSLayoutConstraint.activate([
            passwordTextField.leadingAnchor
                .constraint(equalTo: bottomView.safeAreaLayoutGuide.leadingAnchor, constant: 24),
            passwordTextField.trailingAnchor
                .constraint(equalTo: bottomView.safeAreaLayoutGuide.trailingAnchor, constant: -24),
            passwordTextField.topAnchor
                .constraint(equalTo: emailTextField.safeAreaLayoutGuide.bottomAnchor, constant: 8),
            passwordTextField.heightAnchor.constraint(equalToConstant: 110)
        ])

        NSLayoutConstraint.activate([
            confirmButtom.leadingAnchor
                .constraint(equalTo: bottomView.safeAreaLayoutGuide.leadingAnchor, constant: 24),
            confirmButtom.trailingAnchor
                .constraint(equalTo: bottomView.safeAreaLayoutGuide.trailingAnchor, constant: -24),
            confirmButtom.topAnchor
                .constraint(equalTo: passwordTextField.safeAreaLayoutGuide.bottomAnchor, constant: 8),
            confirmButtom.heightAnchor
                .constraint(equalToConstant: 50),
            confirmButtom.bottomAnchor
                .constraint(lessThanOrEqualTo: bottomView.safeAreaLayoutGuide.bottomAnchor, constant: -10)
        ])
    }

    private func configureStaticComponents() {
        let stackTitleView = UIStackView()
        stackTitleView.axis = .vertical
        stackTitleView.translatesAutoresizingMaskIntoConstraints = false
        stackTitleView.alignment = .leading
        stackTitleView.distribution = .fillProportionally
        self.view.addSubview(stackTitleView)

        let wellcomeTitle =  UILabel.gilroy(type: .bold, text: "Boas vindas,", fontSize: 40)
        let wellcomeSubtitle = UILabel.gilroy(type: .light, text: "Você está no Empresas.", fontSize: 24)

        stackTitleView.addArrangedSubview(wellcomeTitle)
        stackTitleView.addArrangedSubview(wellcomeSubtitle)

        NSLayoutConstraint.activate([
              stackTitleView.topAnchor.constraint(lessThanOrEqualTo: view.topAnchor, constant: 349),
              stackTitleView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
              stackTitleView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 24),
              stackTitleView.heightAnchor.constraint(equalToConstant: 76)
        ])

        self.view.addSubview(self.bottomView)

        NSLayoutConstraint.activate([
            self.bottomView.topAnchor.constraint(equalTo: stackTitleView.bottomAnchor, constant: 31),
            self.bottomView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            self.bottomView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            self.bottomView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            self.bottomView.heightAnchor.constraint(greaterThanOrEqualToConstant: 356)
        ])

    }

    private func configureBackground() {
        guard let image = UIImage(named: Constants.Backgrounds.login) else {return}
        let background = UIImageView(image: image)

        background.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(background)

        let viewSafeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            background.topAnchor.constraint(equalTo: view.topAnchor),
            background.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            background.leadingAnchor.constraint(equalTo: viewSafeArea.leadingAnchor),
            background.trailingAnchor.constraint(equalTo: viewSafeArea.trailingAnchor)
        ])
    }

    private func configureLoadingView() {
        view.addSubview(loadingView)

        NSLayoutConstraint.activate([
            loadingView.topAnchor.constraint(equalTo: view.topAnchor),
            loadingView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            loadingView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            loadingView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }

}
