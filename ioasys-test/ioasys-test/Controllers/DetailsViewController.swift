//
//  DetailsViewController.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 23/04/22.
//

import UIKit
import Kingfisher
import RxCocoa
import RxSwift
class DetailsViewController: UIViewController {
    var enterprise: Enterprise?
    let disposebag = DisposeBag()

    private let headerImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: Constants.Backgrounds.standart))
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let backButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setImage(UIImage(named: Constants.Images.backButton)?
                        .withTintColor(.white, renderingMode: .alwaysOriginal), for: .normal)
        view.isEnabled = true

        return view
    }()

    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        navigationController?.isNavigationBarHidden = true
        buildHeader()
        buildBody()
        bindBackButton()
    }

    private func buildHeader() {
        guard let enterprise = enterprise else { return }

        let labelTop = UILabel.gilroy(type: .bold, text: enterprise.enterpriseName,
                                   fontSize: 24, color: .white)
        let labelBottom = UILabel.gilroy(type: .bold,
                                         text: enterprise.enterpriseType.enterpriseTypeName,
                                         fontSize: 16, color: .white)

        labelTop.translatesAutoresizingMaskIntoConstraints = false
        labelBottom.translatesAutoresizingMaskIntoConstraints = false

        labelTop.textAlignment = .center
        labelBottom.textAlignment = .center

        view.addSubview(headerImage)
        headerImage.addSubview(labelTop)
        headerImage.addSubview(labelBottom)
        view.addSubview(backButton)

        NSLayoutConstraint.activate([
            headerImage.topAnchor.constraint(equalTo: view.topAnchor),
            headerImage.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            headerImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            headerImage.heightAnchor.constraint(equalToConstant: 148)
        ])

        NSLayoutConstraint.activate([
            backButton.topAnchor
                .constraint(equalTo: headerImage.topAnchor, constant: 69),
            backButton.leadingAnchor
                .constraint(equalTo: headerImage.safeAreaLayoutGuide.leadingAnchor, constant: 24),
            backButton.heightAnchor.constraint(equalToConstant: 40),
            backButton.widthAnchor.constraint(equalTo: backButton.heightAnchor)
        ])

        NSLayoutConstraint.activate([
            labelBottom.bottomAnchor
                .constraint(equalTo: headerImage.safeAreaLayoutGuide.bottomAnchor, constant: -24),
            labelBottom.centerXAnchor.constraint(equalTo: headerImage.centerXAnchor)
        ])

        NSLayoutConstraint.activate([
            labelTop.bottomAnchor
                .constraint(equalTo: labelBottom.topAnchor, constant: -4),
            labelTop.centerXAnchor.constraint(equalTo: headerImage.centerXAnchor)
        ])

    }

    private func bindBackButton() {
        backButton.rx.tap.asObservable().subscribe { [weak self] _  in

            self?.navigationController?.popViewController(animated: true)
        }.disposed(by: disposebag)
    }

    private func buildBody() {

        guard let enterprise = enterprise else { return }

        let image = UIImageView()
        image.kf.setImage(with: URL(string: "\(Constants.baseUrl)\(enterprise.photo)"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.roundBorders(borderType: .onlyBotton(corner: 10))

        let text = UILabel.gilroy(type: .medium,
                                  text: enterprise.description,
                                  fontSize: 16, color: .gray)
        text.translatesAutoresizingMaskIntoConstraints = false
        text.numberOfLines = 0
        text.adjustsFontSizeToFitWidth = true

        view.addSubview(image)
        view.addSubview(text)

        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: headerImage.bottomAnchor),
            image.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            image.heightAnchor.constraint(equalToConstant: 244)
        ])

        NSLayoutConstraint.activate([
            text.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 31),
            text.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 24),
            text.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -24),
            text.bottomAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -24)
        ])

    }

}
