//
//  EnterprisesViewController.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 21/04/22.
//

import UIKit
import RxSwift
import RxCocoa
import RxRelay

class EnterprisesViewController: UIViewController {
    var coordinator: Coordinator?
    var viewModel: EnterprisesViewModel?
    let disposebag = DisposeBag()

    fileprivate lazy var kCellWidth: Int = Int(self.view.bounds.width - 48 / 2)
    fileprivate var kCellHeight: Int = 144

    private let searchBar: UITextField = {
        let view = UITextField()
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.cornerRadius = 10

        var configuration = UIButton.Configuration.plain()
        configuration.imagePadding = -2
        configuration.baseBackgroundColor = .clear

        let image = UIButton(configuration: configuration, primaryAction: nil)
        image.frame = CGRect(x: 0, y: 0, width: 16, height: 16)
        image.setImage(UIImage(systemName: "magnifyingglass")?
                        .withTintColor(.gray, renderingMode: .alwaysOriginal), for: .normal)

        image.isEnabled = false

        view.leftView = image
        view.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        view.rightViewMode = .always
        view.leftViewMode = .always
        view.placeholder = "Buscar..."
        view.sizeToFit()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let width = Int((UIScreen.main.bounds.width - 80) / 2)
        let height = Int(Float(width) * 0.95)
        layout.itemSize = CGSize(width: width, height: height)
        layout.sectionInset = UIEdgeInsets(top: 1, left: 5, bottom: 0, right: 5)
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 20

        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.showsVerticalScrollIndicator = false
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(EnterprisesCollectionViewCell.self,
                      forCellWithReuseIdentifier: EnterprisesCollectionViewCell.identifier)
        view.backgroundColor = .white
        return view
    }()

    private let notFoundStack: UIStackView = {
        let view = UIStackView()
        view.spacing = 1
        view.axis = .vertical
        view.translatesAutoresizingMaskIntoConstraints = false
        view.distribution = .fillProportionally
        view.alignment = .center
        let image = UIImageView(image: UIImage(named: Constants.Images.searchImage))
        let text = UILabel.gilroy(type: .medium, text: "Empresa não encontrada",
                                  fontSize: 16, color: .gray)

        text.textAlignment = .center
        view.addArrangedSubview(image)
        view.addArrangedSubview(text)

        view.isHidden = true
        return view
    }()

    private let navigationButton: UIBarButtonItem = {
        let button = UIBarButtonItem()
        button.image = UIImage(systemName: "arrow.left")?
            .withTintColor(UIColor(named: Constants.Colours.purple) ?? UIColor.magenta, renderingMode: .alwaysOriginal)

        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Pesquise por uma empresa"
        view.backgroundColor = .white
        navigationController?.isNavigationBarHidden = false
        navigationItem.setValue(1, forKey: "__largeTitleTwoLineMode")
        navigationController?.navigationBar.prefersLargeTitles = true

        bindNavigationButton()
        configureComponentsView()
        configureNotFoundStack()
        bindCollectionViewData()
        bindSelectedEnterprise()
        bindSearchBar()
        guard let viewModel = viewModel else { return }
        viewModel.fetchEnterprises()
    }

    private func configureNotFoundStack() {
        view.addSubview(notFoundStack)

        NSLayoutConstraint.activate([
            notFoundStack.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 80),
            notFoundStack.leadingAnchor
                .constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 80),
            notFoundStack.trailingAnchor
                .constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -80),
            notFoundStack.heightAnchor.constraint(equalToConstant: 192)

        ])
    }

    private func bindCollectionViewData() {
        viewModel?.selectedEnterprises.asObservable()
            .bind(to: self.collectionView.rx.items(cellIdentifier: EnterprisesCollectionViewCell.identifier,
                                                   cellType: EnterprisesCollectionViewCell.self)) { _, data, cell in
                cell.configureData(title: data.enterpriseName, imagePath: data.photo)
        }
        .disposed(by: disposebag)

        viewModel?.selectedEnterprises.asDriver(onErrorRecover: { _ in
            return Driver<[Enterprise]>.empty()
        }).drive(onNext: { [weak self] enterprise in
            self?.notFoundStack.isHidden = enterprise.count > 0
        }).disposed(by: disposebag)
    }

    private func bindSelectedEnterprise() {

        collectionView.rx.itemSelected.subscribe(onNext: { [weak self] element in
            guard let viewModel = self?.viewModel else { return }

            let detail = viewModel.findByPosition(position: element.item)
            self?.coordinator?.next(screen: .details(enterprise: detail))
        }).disposed(by: disposebag)
    }

    private func bindNavigationButton() {

        navigationButton.rx.tap.asObservable().subscribe({ [weak self] _ in
            self?.searchBar.text = ""
        }).disposed(by: disposebag)
    }

    private func bindSearchBar() {
        searchBar.rx.text.changed.asDriver().drive(onNext: { [weak self] text in
            guard let text = text else {return}
            self?.isSearchBarEmpty(text.isEmpty)
            self?.viewModel?.filter(by: text)
        }).disposed(by: disposebag)

        searchBar.rx.controlEvent([.editingDidEndOnExit, .editingDidEnd])
            .subscribe { [weak self] _ in
                self?.navigationController?.navigationBar.prefersLargeTitles = true
                self?.title = "Pesquise por uma empresa"
                self?.navigationItem.leftBarButtonItem = nil

            }.disposed(by: disposebag)

        searchBar.rx.controlEvent([.editingDidBegin])
            .subscribe { [weak self] _ in
                self?.navigationController?.navigationBar.prefersLargeTitles = false
                self?.title = "Pesquise"
                self?.navigationItem.setLeftBarButton(self?.navigationButton, animated: false)
            }.disposed(by: disposebag)
    }

    private func isSearchBarEmpty(_ isEmpty: Bool) {
        searchBar.layer.borderColor = isEmpty ? UIColor.gray.cgColor :
        (UIColor(named: Constants.Colours.purple) ?? UIColor.magenta).cgColor
    }

    private func configureComponentsView() {
        view.addSubview(searchBar)
        view.addSubview(collectionView)

        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,
                                                    constant: 24),
            searchBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,
                                                     constant: -24),
            searchBar.heightAnchor.constraint(equalToConstant: 48)
        ])

        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 10),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,
                                                    constant: 23),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,
                                                     constant: -23)
        ])
    }

}
