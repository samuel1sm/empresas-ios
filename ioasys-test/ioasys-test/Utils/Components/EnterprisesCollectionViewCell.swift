//
//  EnterprisesCollectionViewCell.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 21/04/22.
//

import UIKit
import Kingfisher

class EnterprisesCollectionViewCell: UICollectionViewCell {
    static let identifier = "EnterprisesCollectionViewCell"
    private let image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.layer.shadowOpacity = 0.5
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.2, height: 2)
        view.roundBorders(borderType: .onlyTop(corner: 10))
        return view
    }()

    private let title: UILabel = {
        let view = UILabel.gilroy(type: .medium, text: "", fontSize: 17, color: .gray)
        view.textAlignment = .center
        view.adjustsFontSizeToFitWidth = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildCell()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureData(title: String, imagePath: String) {
        self.title.text = title
        image.kf.setImage(with: URL(string: "\(Constants.baseUrl)\(imagePath)"))
    }

    private func buildCell() {
        self.backgroundColor = .clear
        let background = UIImageView(image: UIImage(named: Constants.Backgrounds.standart))
        background.translatesAutoresizingMaskIntoConstraints = false
        background.contentMode = .scaleToFill
        background.roundBorders(borderType: .both(corner: 10))

        let bottomView = UIView()
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.roundBorders(borderType: .onlyBotton(corner: 10), clipsToBounds: false)
        bottomView.addSubview(title)
        bottomView.layer.shadowOpacity = 0.5
        bottomView.layer.shadowColor = UIColor.black.cgColor
        bottomView.layer.shadowOffset = CGSize(width: 0.2, height: 2)
        bottomView.backgroundColor = .white

        self.addSubview(background)
        self.addSubview(image)
        self.addSubview(bottomView)

        NSLayoutConstraint.activate([
            background.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 32),
            background.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            background.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor),
            background.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor)
        ])

        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            image.bottomAnchor.constraint(equalTo: bottomView.topAnchor),
            image.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 32),
            image.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -32)
        ])

        NSLayoutConstraint.activate([
            bottomView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            bottomView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor),
            bottomView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor),
            bottomView.heightAnchor.constraint(equalToConstant: 33)
        ])

        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: bottomView.safeAreaLayoutGuide.topAnchor, constant: 8),
            title.bottomAnchor
                .constraint(equalTo: bottomView.safeAreaLayoutGuide.bottomAnchor, constant: -8),
            title.leadingAnchor.constraint(equalTo: bottomView.safeAreaLayoutGuide.leadingAnchor),
            title.trailingAnchor.constraint(equalTo: bottomView.safeAreaLayoutGuide.trailingAnchor)
        ])

    }

}
