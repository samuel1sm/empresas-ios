//
//  UILabelComponents.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 18/04/22.
//

import UIKit

extension UILabel {
    static func gilroy(type: Constants.Fonts, text: String, fontSize: CGFloat, color: UIColor = .white) -> UILabel {
        let label = UILabel()
        label.font = UIFont(name: type.rawValue, size: fontSize)
        label.text = text
        label.textColor = color
        return label
    }
}
