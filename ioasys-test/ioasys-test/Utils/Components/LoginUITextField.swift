//
//  LoginUITextField.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 18/04/22.
//

import UIKit

class LoginUITextField: UIView {
    private let topText: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()

    private let bottomText: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = " "
        return label
    }()

    private(set) var textField: UITextField = {
        let peddingView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 48))
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = 8
        textField.layer.borderColor = UIColor.gray.cgColor
        textField.layer.borderWidth = 1
        textField.leftView = peddingView
        textField.rightView = peddingView
        textField.leftViewMode = .always
        textField.rightViewMode = .always
        return textField
    }()

    private var textWasWrong = false

    var placeholderText: String {
        get {
            return self.textField.placeholder ?? ""
        }

        set {
            self.textField.placeholder = newValue
            self.topText.text = newValue
        }
    }

    var textfieldType: UITextContentType {
        get {
            return self.textField.textContentType
        }

        set {
            textField.textContentType = newValue

            if newValue == .password {
                self.textField.isSecureTextEntry = true
                self.configureRightButton()
            }
        }
    }

    var bottonText: String {
        get {
            return self.bottomText.text ?? ""
        }

        set {
            self.bottomText.text = newValue
        }
    }

    var text: String? {
        get {
            return textField.text
        }

        set {
            textField.text = newValue
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildCompent()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func activateTopText(_ activate: Bool) {
        let color: UIColor = textWasWrong ? .red : .black

        topText.textColor = activate ? color : .white
    }

    func isTextWrong(_ isWrong: Bool) {
        textWasWrong = isWrong

        if textWasWrong {
            topText.textColor = .red
            textField.textColor = .red
            textField.tintColor = .red
            textField.layer.borderColor = UIColor.red.cgColor
            bottomText.textColor = .red
            return
        }

        topText.textColor = .black
        textField.textColor = .black
        textField.tintColor = .gray
        textField.layer.borderColor = UIColor.red.cgColor
        bottomText.textColor = .white
    }

    func isSelected(_ isSelected: Bool) {
        let previusColor: UIColor = textWasWrong ? .red : .gray
        textField.layer.borderColor = isSelected ?
        (UIColor(named: Constants.Colours.purple) ?? UIColor.magenta).cgColor : previusColor.cgColor
    }

    private func configureRightButton() {
        var configuration = UIButton.Configuration.plain()
        configuration.imagePadding = 2
        configuration.baseBackgroundColor = .clear

        let hideButton = UIButton(configuration: configuration, primaryAction: nil)
        hideButton.frame = CGRect(x: 0, y: 0, width: 16, height: 16)

        hideButton.setImage(UIImage(systemName: "eye.slash")?
                                .withTintColor(.gray, renderingMode: .alwaysOriginal), for: .normal)
        hideButton.setImage(UIImage(systemName: "eye")?
                                .withTintColor(.gray, renderingMode: .alwaysOriginal), for: .selected)
        hideButton.addTarget(self, action: #selector(changeButtonType(_:)), for: .touchUpInside)
        hideButton.isSelected = !hideButton.isSelected

        self.textField.rightView = hideButton
    }

    private func buildCompent() {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.distribution = .fillProportionally
        self.addSubview(stack)

        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: self.topAnchor),
            stack.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            stack.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stack.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])

        stack.addArrangedSubview(topText)
        stack.addArrangedSubview(textField)
        stack.addArrangedSubview(bottomText)
    }

    @objc private func changeButtonType(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.textField.isSecureTextEntry = sender.isSelected
    }

}
