//
//  Utils.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 18/04/22.
//

import Foundation

struct Constants {
    struct Backgrounds {
        static let login: String = "Background_login"
        static let standart: String = "Background"
    }

    struct Images {
        static let backButton: String = "BackButton"
        static let searchImage: String = "SearchImage"
        static let loading: String = "Loading"
    }

    struct Colours {
        static let purple: String = "Purple"
    }

    static let baseUrl = "https://empresas.ioasys.com.br/"

    enum Fonts: String {
        case bold = "Gilroy-Bold"
        case light = "Gilroy-Light"
        case medium = "Gilroy-Medium"

    }
}
