//
//  UIView+Extentions.swift
//  ioasys-test
//
//  Created by FRANCISCO SAMUEL DA SILVA MARTINS on 23/04/22.
//

import UIKit

extension UIView {
    enum BorderTypes {
        case onlyTop(corner: CGFloat)
        case onlyBotton(corner: CGFloat)
        case both(corner: CGFloat)
    }

    func roundBorders(borderType: BorderTypes, clipsToBounds: Bool = true) {
        self.clipsToBounds = clipsToBounds

        switch borderType {
        case .onlyTop(let corner):
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            self.layer.cornerRadius = corner
        case .onlyBotton(let corner):
            self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            self.layer.cornerRadius = corner
        case .both(let corner):
            self.layer.cornerRadius = corner
        }
    }
}
